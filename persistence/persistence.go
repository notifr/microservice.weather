package persistence

import (
	"log"

	"fmt"

	"zhaw.ch/microservice.weather/model"
)

var subs model.Subscriptions

//Store a Subscription to array
func Store(sub model.Subscription) bool {
	return appendSubscription(sub)
}

func appendSubscription(sub model.Subscription) bool {
	log.Println("received following data to store: " + sub.SubInfo.City + " " + sub.ID)
	if containsSubscription(sub) {
		fmt.Println("subscription already exist in subs")
		return false
	}
	if sub.SubInfo.City != "" && sub.ID != "" {

		subs.Subscription = append(subs.Subscription, sub)
		return containsSubscription(sub)
	}
	log.Println("not enough arguments!")
	return false
}

func containsSubscription(subscription model.Subscription) bool {
	for _, sub := range subs.Subscription {
		if sub.ID == subscription.ID {
			return true
		}
	}
	return false
}

// GetSubscriptions as array of Subscription
func GetSubscriptions() []model.Subscription {
	return subs.Subscription

}

// RemoveSubscription removes a given Unsubscription from subscription array (currently not implemented properly but it works)
func RemoveSubscription(unsub string) bool {

	for i := len(subs.Subscription) - 1; i >= 0; i-- {
		sub := subs.Subscription[i]
		// Condition to decide if current element has to be deleted:
		if sub.ID == unsub {
			subs.Subscription = append(subs.Subscription[:i], subs.Subscription[i+1:]...)
			log.Println("Content of subs after deleting: ", subs)
			return true
		}
	}
	return false
}
