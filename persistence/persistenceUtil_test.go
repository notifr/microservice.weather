package persistence

import (
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"zhaw.ch/microservice.weather/model"
)

func Test_ReadConfig_Fail(t *testing.T) {
	var sub model.Subscription
	res := Store(sub)
	if res {
		t.Fatalf("should have an error: %v", res)
	} else {
		t.Log("succes, able to handle empty sub object")
	}
}

func Test_StoreArtist_Success(t *testing.T) {
	var sub model.Subscription
	sub.ID = "1235"
	sub.SubInfo.City = "Winterthur"
	res := Store(sub)
	if !res {
		t.Fatalf("shouldn't have an error: %v", res)
	} else {
		t.Log("succes, able to handle a sub object")
	}
}

func Test_ReadConfig_Success(t *testing.T) {
	var sub model.Subscription
	timestamp := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)
	sub.ID = timestamp
	sub.SubInfo.City = "Winterthur"
	success := appendSubscription(sub)
	if !success {
		t.Fatalf("shouldn't have an error")
	} else {
		t.Log("succes, able to handle a sub object")
	}
}

func Test_RemoveSubscription_Success(t *testing.T) {
	var sub model.Subscription
	timestamp := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)
	sub.ID = timestamp
	sub.SubInfo.City = "Winterthur"
	success := appendSubscription(sub)
	if !success {
		t.Fatalf("shouldn't have an error")
	} else {
		t.Log("succes, able to handle a sub object")
	}

	removed := RemoveSubscription(sub.ID)
	if !removed {
		t.Fatalf("shouldn't have an error")
	} else {
		t.Log("succes, able to handle a sub object")
	}
}

func Test_RemoveSubscription_Fail(t *testing.T) {
	var sub model.Subscription
	timestamp := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)
	sub.ID = timestamp
	removed := RemoveSubscription(sub.ID)
	assert.Equal(t, false, removed)
}
