package main

import (
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"zhaw.ch/microservice.weather/configuration"
	"zhaw.ch/microservice.weather/model"
	"zhaw.ch/microservice.weather/persistence"
)

func Test_executeStartupProcedure_Fail(t *testing.T) {
	var config configuration.Config
	err := executeStartupProcedure(&config)
	assert.NotNil(t, err)
}

func Test_executeStartupProcedure_Success(t *testing.T) {
	config, err := configuration.ReadConfig()
	assert.Nil(t, err)
	err = executeStartupProcedure(&config)
	assert.Nil(t, err)
}

func Test_weatherman_Fail(t *testing.T) {
	var config configuration.Config
	insertTestData(t)
	err := weatherman(&config)
	assert.NotNil(t, err)
}

func Test_weatherman_Success(t *testing.T) {
	config, err := configuration.ReadConfig()
	assert.Nil(t, err)
	insertTestData(t)
	err = weatherman(&config)
	assert.Nil(t, err)
}

func insertTestData(t *testing.T) {
	timestamp := strconv.FormatInt(time.Now().UTC().UnixNano(), 10)
	var sub model.Subscription
	sub.ID = timestamp
	sub.SubInfo.City = "Wil"
	assert.True(t, persistence.Store(sub))
}
