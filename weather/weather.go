package weather

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	"zhaw.ch/microservice.weather/configuration"
	"zhaw.ch/microservice.weather/model"
)

var (
	weatherKeys = map[string]bool{"main": false, "wind": false, "coord": false, "weather": true, "sys": false, "clouds": false}
)

const (
	dumpRaw = false
	api     = "5c288accfca15e8f1a35bfa4f44a25ef"
)

// CurrentWeather gets the Current Weather for a given city
func CurrentWeather(config *configuration.Config, city string) (string, error) {
	urlString := fmt.Sprintf(config.WeatherURL, city, api)
	u, err := url.Parse(urlString)
	if err != nil {
		return "", err
	}
	res, err := http.Get(u.String())
	if err != nil {
		return "", err
	}
	jsonBlob, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return "", err
	}
	var data model.WeatherData

	if dumpRaw {
		fmt.Printf("blob = %s\n\n", jsonBlob)
	}
	err = json.Unmarshal(jsonBlob, &data)
	if err != nil {
		fmt.Println("error:", err)
		return "", err
	}
	weather := "Das Wetter in " + data.Name + " ist " + data.WeatherAsStruct[0].Weather + " bei " + FloatToString(data.Temp.Temp) + "°C Höchsttemperatur " + FloatToString(data.Temp.TempMax) + "°C Tiefsttemperatur " + FloatToString(data.Temp.TempMin) + "°C"

	return weather, err
}

// FloatToString converts a given float64 to a string with two chars after .
func FloatToString(input_num float64) string {
	// to convert a float number to a string
	return strconv.FormatFloat(input_num, 'f', 0, 64)
}
