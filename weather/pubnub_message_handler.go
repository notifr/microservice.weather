package weather

import (
	"encoding/json"
	"fmt"

	"zhaw.ch/microservice.weather/configuration"
	"zhaw.ch/microservice.weather/model"
	"zhaw.ch/microservice.weather/persistence"
)

//HandleMessage received from Pubnub Store/Publish (Subscriptions)
func HandleMessage(config *configuration.Config, message string) error {
	msg := []byte(message)
	var msgType model.Message
	err := UnmarshalSubscription(msg, &msgType)
	fmt.Println(msgType.Type)
	if msgType.Type == config.Subscription || msgType.Type == config.Subscriptions {
		var data model.SubscriptionData
		err = UnmarshalSubscription(msg, &data)
		fmt.Println(data.SubscriptionData.Subscription)
		for _, sub := range data.SubscriptionData.Subscription {
			fmt.Println("save Subscription " + sub.SubInfo.City)
			persistence.Store(sub)
		}
	}
	if msgType.Type == config.Unsubscription {
		var data model.Unsubscribe
		fmt.Println("deleting " + message)
		err = UnmarshalSubscription(msg, &data)
		for _, unsubscription := range data.Unsubscribe.UnSubscriptions {
			fmt.Println(unsubscription.ID + " is")
			if !persistence.RemoveSubscription(unsubscription.ID) {
				fmt.Println("error deleting subscription")
			}
		}

	}
	return err
}

// UnmarshalSubscription from pubnub
func UnmarshalSubscription(msg []byte, v interface{}) error {
	err := json.Unmarshal(msg, v)
	if err != nil {
		fmt.Println("not possible to unmarshal stream", msg, err)
		return err
	}
	return nil
}
