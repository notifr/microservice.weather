package weather

import (
	"zhaw.ch/microservice.weather/configuration"
	"zhaw.ch/microservice.weather/pubnub"
)

// ListenOnPubnub start listening on Pubnub
func ListenOnPubnub(config *configuration.Config) {
	for true {
		message, success := pubnub.Subscribe(config)
		if success {
			HandleMessage(config, message)
		}
	}
}
