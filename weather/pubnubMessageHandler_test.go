package weather

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zhaw.ch/microservice.weather/configuration"
	"zhaw.ch/microservice.weather/model"
)

func Test_HandleMessage_Fail(t *testing.T) {
	config, err := configuration.ReadConfig()
	err = HandleMessage(&config, "")
	if err == nil {
		t.Fatalf("There should be a error: %v", err)
	}
}

func Test_HandleMessage_Success(t *testing.T) {
	config, err := configuration.ReadConfig()
	err = HandleMessage(&config, `{"message":"subscribe","data":{"subscriptions":[{"data":{"band":"Airbourne","country":"CH"},"id":"58d930a89bcb84000805d355"}]}}`)
	if err != nil {
		t.Fatalf("There shouldn't be a error: %v", err)
	}
}

func Test_HandleMessage_Fail_onData(t *testing.T) {
	config, err := configuration.ReadConfig()
	err = HandleMessage(&config, `{"message":"subscribe","data":{"subscriptions":[{"data":{"ba≤≤&%ç*%&/Hgtz6d5ndAirbourne","country":"CH"},"id":"58d930a89bcb84000805d355"}]}}`)
	if err == nil {
		t.Fatalf("There should be a error: %v", err)
	}
}

func Test_UnmarshalSubscription_Fail(t *testing.T) {
	var msg []byte
	var data model.SubscriptionData
	err := UnmarshalSubscription(msg, data)
	if err == nil {
		t.Fatalf("There should be a error: %v", err)
	}
}

func Test_UnmarshalSubscription_Success(t *testing.T) {
	var msg []byte
	msg = []byte(`{  "message": "werde informiert wenn mastodon spielt NEU2",  "userid": "58e11854d0d5a5d61369c824",  "data": {    "country": "CH","band": "Destruction"  },  "connector": "bands"}`)
	var data model.SubscriptionData
	err := UnmarshalSubscription(msg, &data)
	if err != nil {
		t.Fatalf("There shouldnt be a error: %v", err)
	}
}

func Test_FloatToString_Success(t *testing.T) {
	res := FloatToString(8.252)
	assert.Equal(t, "8", res)
}

func Test_FloatToString_Fail(t *testing.T) {
	res := FloatToString(8.752)
	assert.Equal(t, "9", res)
}
