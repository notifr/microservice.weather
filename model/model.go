package model

//Message type used to receive and publish
type Message struct {
	Type string `json:"message"`
}

// Subscription definition
type Subscription struct {
	ID      string `json:"id"`
	SubInfo Data   `json:"data"`
}

// Data containing SubscriptionInfo
type Data struct {
	City string `json:"city"`
}

// Subscriptions are an array of Subscription
type Subscriptions struct {
	Subscription []Subscription `json:"subscriptions"`
}

// SubscriptionData contains the data Block of the subscriptions
type SubscriptionData struct {
	SubscriptionData Subscriptions `json:"data"`
}

// Connector holds information about the connector
type Connector struct {
	Connector string `json:"connector"`
}

// Startup Message
type Startup struct {
	Message   string    `json:"message"`
	Connector Connector `json:"data"`
}

// Unsubscribe is a type struct ob unsubscribe
type Unsubscribe struct {
	Unsubscribe UnSubscriptionData `json:"data"`
}

//UnSubscriptionData contains an Array of UnSubscriptions
type UnSubscriptionData struct {
	UnSubscriptions []UnSubscription `json:"subscriptions"`
}

// UnSubscription is the ID that has to be unsubscribed
type UnSubscription struct {
	ID string `json:"id"`
}

//Notification which contains the message "notify"
//and the NotificationData
type Notification struct {
	Message string           `json:"message"`
	Data    NotificationData `json:"data"`
}

// NotificationData contains the message and
//the NotifySubscriptions which have to be notified
type NotificationData struct {
	NotificationMessage string               `json:"notificationMessage"`
	NotifySubscriptions []NotifySubscription `json:"subscriptions"`
}

// NotifySubscription that has to be notified
type NotifySubscription struct {
	ID string `json:"id"`
}

// SubscriptionToNotify (used for new Subscriptions)
type SubscriptionToNotify struct {
	IsNew          bool
	SubscriptionID string
}

// WeatherData (used for new Subscriptions)
type WeatherData struct {
	WeatherAsStruct []WeatherInfo `json:"weather"`
	Temp            Temperature   `json:"main"`
	Name            string        `json:"name"`
}

// WeatherInfo in json
type WeatherInfo struct {
	Weather string `json:"description"`
}

// Temperature in json
type Temperature struct {
	Temp    float64 `json:"temp"`
	TempMax float64 `json:"temp_max"`
	TempMin float64 `json:"temp_min"`
}
