package pubnub

import (
	"encoding/json"
	"fmt"

	"github.com/pubnub/go/messaging"
	"zhaw.ch/microservice.weather/configuration"
)

//Subscribe to channel needs Configuration
func Subscribe(config *configuration.Config) (string, bool) {
	if config.Publish_key == "" || config.Subscribe_key == "" {
		return "", false
	}
	successChannel := make(chan []byte)
	errorChannel := make(chan []byte)
	pubnub := messaging.NewPubnub(config.Publish_key, config.Subscribe_key, "", "", false, "", nil)
	pubnub.Subscribe(config.Channel, "", successChannel, false, errorChannel)
	for {
		select {
		case response := <-successChannel:
			var msg []interface{}
			err := json.Unmarshal(response, &msg)
			if err != nil {
				fmt.Println(err)
			}
			switch m := msg[0].(type) {
			case float64:
				fmt.Println(msg[1].(string))
			case []interface{}:
				fmt.Printf("Received message '%s' on channel '%s'\n ", m[0], msg[2])
				message := fmt.Sprint(m[0])
				return message, true
			default:
				panic(fmt.Sprintf("Unknown type: %T", m))
			}
		case err := <-errorChannel:
			fmt.Println(string(err))
			return "", false
		case <-messaging.SubscribeTimeout():
			fmt.Println("Subscribe() timeout")
			return "", false
		}
	}
}
