package pubnub

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zhaw.ch/microservice.weather/configuration"
)

func Test_sayHello_Fail(t *testing.T) {
	var config configuration.Config
	res, success := Subscribe(&config)
	assert.Equal(t, false, success)
	assert.Equal(t, "", res)
}
