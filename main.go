package main

import (
	"log"

	"zhaw.ch/microservice.weather/configuration"
	"zhaw.ch/microservice.weather/publisher"
	"zhaw.ch/microservice.weather/weather"

	// Shortening the import reference name seems to make it a bit easier

	"github.com/robfig/cron"
	"zhaw.ch/microservice.weather/persistence"
)

func main() {
	config, err := configuration.ReadConfig()
	if err != nil {
		log.Fatal("not able to read Configuration", err)
	}
	if executeStartupProcedure(&config) != nil {
		log.Fatal("Startup failed with: ", err)
	}
	go func() { weather.ListenOnPubnub(&config) }()
	weatherman(&config)
	scheduler := cron.New()
	scheduler.AddFunc("0 0 6 * * *", func() { weatherman(&config) })
	scheduler.Run()
}

func executeStartupProcedure(config *configuration.Config) error {
	response, err := publisher.SayHello(config)
	if err != nil {
		log.Println("not able to send hello to Backend", err)
		return err
	}
	log.Println(response)
	err = weather.HandleMessage(config, response)
	if err != nil {
		log.Fatal("not able to store subs", err)
		return err
	}
	example, err := weather.CurrentWeather(config, "Winterthur")
	log.Print(example)
	weatherman(config)
	return err
}

func weatherman(config *configuration.Config) error {
	log.Println("started the weatherman")
	var err error
	subscriptions := persistence.GetSubscriptions()
	log.Println(subscriptions)
	forecast := ""
	for _, subscription := range subscriptions {
		currentWeather, err := weather.CurrentWeather(config, subscription.SubInfo.City)
		if err != nil {
			log.Println(err)
			return err
		}
		err = publisher.Publish(config, currentWeather+" "+forecast, subscription)
		log.Println(currentWeather + " " + forecast)
	}
	return err
}
