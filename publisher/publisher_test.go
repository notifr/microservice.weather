package publisher

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"zhaw.ch/microservice.weather/configuration"
	"zhaw.ch/microservice.weather/model"
)

func Test_sendHTTPPost_Fail(t *testing.T) {
	var config configuration.Config

	err := sendHTTPPost(&config, []byte("Here is a string...."))
	assert.NotEqual(t, nil, err)
}

// needs internetconnection
func Test_sendHTTPPost_Success(t *testing.T) {
	config, err := configuration.ReadConfig()
	assert.Nil(t, err)
	err = sendHTTPPost(&config, []byte(`{"message":"notify","data":{"notificationMessage":"The weather in Winterthur is Clouds at 19.59°C max is 20°C min is
scriptions":[{"id":"590b962d21ff6200f60aba46"}]}}`))
	assert.Equal(t, nil, err)
}

func Test_sendHTTPThread_Success(t *testing.T) {
	config, err := configuration.ReadConfig()
	assert.Nil(t, err)
	err = sendHTTPThread(&config, []byte(`{"message":"notify","data":{"notificationMessage":"The weather in Winterthur is Clouds at 19.59°C max is 20°C min is
scriptions":[{"id":"590b962d21ff6200f60aba46"}]}}`))
	assert.Equal(t, nil, err)
}

func Test_createNotification_Success(t *testing.T) {
	testMessage := "This is only for testing purpose"
	var subToNotify model.Subscription
	res, err := createNotification(testMessage, subToNotify)
	assert.Nil(t, err)
	assert.Equal(t, testMessage, res.Data.NotificationMessage)
}
