package publisher

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"zhaw.ch/microservice.weather/configuration"
	"zhaw.ch/microservice.weather/model"
)

// SayHello sends a http post to Backend
func SayHello(config *configuration.Config) (string, error) {

	response, err := http.Get(config.HostBackend + config.NotifRBackend)
	if err != nil || response == nil {
		log.Println("There was an error in getting Request using "+config.NotifRBackend+"  ", err)
		return "", err
	}
	fmt.Println(response)
	defer response.Body.Close()
	res, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
	}

	return string(res), err
}

// Publish a given message
func Publish(config *configuration.Config, message string, sub model.Subscription) error {
	notification, err := createNotification(message, sub)
	if err != nil {
		fmt.Println("not able to create Notification", err)
		return err
	}
	notificationAsByteArray, err := json.Marshal(notification)
	if err != nil {
		fmt.Println("not able to marshal notification", err)
		return err
	}
	go func() { sendHTTPThread(config, notificationAsByteArray) }()
	return err
}

func sendHTTPThread(config *configuration.Config, notificationAsByteArray []byte) error {
	err := sendHTTPPost(config, notificationAsByteArray)
	if err != nil {
		fmt.Println("not able to send http POST", err)
	}
	return err
}

func createNotification(message string, subToNotify model.Subscription) (model.Notification, error) {
	var notification model.Notification
	notification.Message = "notify"
	notification.Data.NotificationMessage = message
	var notifySubscription model.NotifySubscription
	notifySubscription.ID = subToNotify.ID
	notification.Data.NotifySubscriptions = append(notification.Data.NotifySubscriptions, notifySubscription)
	return notification, nil
}
func sendHTTPPost(config *configuration.Config, jsonStr []byte) error {
	req, err := http.NewRequest("POST", config.HostBackend+config.NotifRPostBackend, bytes.NewBuffer(jsonStr))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Cookie", "PHPSESSID=84518ae1db8595eda8242e137b886c7e")
	fmt.Println("notification as json: " + string(jsonStr))
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error executing request: ", err)
		return err
	}

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))

	return err
}
