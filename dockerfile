FROM golang:latest
RUN mkdir /app
ARG ENV
ARG BACKEND-HOST
ADD src/zhaw.ch/microservice.weather/main /app/

RUN mkdir -p /go/src/zhaw.ch/microservice.weather/configuration
ADD src/zhaw.ch/microservice.weather/configuration/config.toml /go/src/zhaw.ch/microservice.weather/configuration
ENV ENV=$ENV
ENV BACKEND-HOST=$BACKEND-HOST
WORKDIR /app
CMD ["/app/main"]
